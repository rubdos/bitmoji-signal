# Bitmoji-to-Signal

Generate a Signal sticker pack from your Bitmoji avatar

## Generating your sticker pack

To obtain your avatar id, login on bitmoji.com, and check the URL of any sticker.
The format of the URL of a sticker is:
https://sdk.bitmoji.com/me/sticker/AVATAR_ID/sticker_number.png?p=...v1&size=thumbnail

To obtain your Signal username and password, look at https://github.com/signalstickers/signalstickers-client/issues/15

Run `SIGNAL_USERNAME=xxx SIGNAL_PASSWORD=yyy python generate-pack.py [AVATAR_ID]`

## Updating sticker index

Only necessary to update the sticker index file.

1. Login using Firefox on bitmoji.com.
2. Dump the contents of the `sticker-grid` div into inner.html
3. Run `python process-inner.py`
