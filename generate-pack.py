#!/usr/bin/env python

import os
import sys
import pickle
import anyio
import aiohttp
from signalstickers_client import StickersClient
from signalstickers_client.models import LocalStickerPack, Sticker

# https://sdk.bitmoji.com/me/sticker/AVATAR_ID/sticker.png?p=something.v1&amp;size=thumbnail
# https://sdk.bitmoji.com/me/sticker/AVATAR_ID/STICKER_ID.png

async def main():
    ids = pickle.load(open('stickers.pickle', 'rb'))
    print("Loaded", len(ids), "sticker identifiers")



    packs = []

    def add_pack():
        part = len(packs) + 1
        pack = LocalStickerPack()
        pack.title = "Bitmoji part " + str(part)
        print(pack.title)
        pack.author = "Bitmoji user"
        packs.append(pack)
    add_pack()

    avatar_id = sys.argv[1]

    async with StickersClient(os.environ['SIGNAL_USERNAME'], os.environ['SIGNAL_PASSWORD']) as client:
        async with aiohttp.ClientSession() as session:
            async def sticker_from_url(url):
                stick = Sticker()
                stick.id = packs[-1].nb_stickers
                async with session.get(url) as response:
                    stick.image_data = await response.read()
                return stick
            # Download cover

            url = "https://sdk.bitmoji.com/me/sticker/" + avatar_id
            cover = await sticker_from_url(url)
            packs[-1].cover = cover

            i = 0
            for sticker_alt, sticker_id in ids.items():
                url = "https://sdk.bitmoji.com/me/sticker/" + avatar_id + "/" + sticker_id
                sticker = await sticker_from_url(url);
                sticker.emoji = sticker_alt # XXX This is not cool.
                packs[-1]._addsticker(sticker)
                i += 1
                if i >= 50:
                    add_pack()
                    packs[-1].cover = cover
                print(i, sticker_alt)

        # Upload the pack
        for pack in packs:
            pack_id, pack_key = await client.upload_pack(pack)
            print("Pack uploaded!\n\nhttps://signal.art/addstickers/#pack_id={}&pack_key={}".format(pack_id, pack_key))
            await anyio.sleep(20)


if __name__ == '__main__':
    anyio.run(main)
