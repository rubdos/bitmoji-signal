#!/usr/bin/env python

from bs4 import BeautifulSoup
from urllib import parse
import pickle

f = open('inner.html', 'r')
stickers = open('stickers.pickle', 'bw+')

stickermap = {}

soup = BeautifulSoup(f)

buttons = soup.find_all('button')
for button in buttons:
    img = button.find('img')
    if img is None or img['src'] is None:
        continue
    url = parse.urlparse(img['src'])
    if not img.has_attr('alt'):
        continue
    alt = img['alt']
    fn = url.path.split('/')[-1]
    stickermap[alt] = fn

pickle.dump(stickermap, stickers)

print("Dumped", len(stickermap), "stickers")
